package test.logger;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.mbe.utilities.MyVars;
import com.mbe.utilities.MyWebAction;
import com.mbe.utilities.Utils;

public class CustomListener implements ITestListener {

	// Called when the test-method execution starts
	@Override
	public void onTestStart(ITestResult result) {
		MyVars.logger
				.info("=================================================================================");
		MyVars.logger.info("Test method started: " + result.getName() + " - "
				+ Utils.getCurrentTime());
		MyVars.logger
				.info("=================================================================================");
	}

	// Called when the test-method execution is a success
	@Override
	public void onTestSuccess(ITestResult result) {
		MyWebAction.takeScreenShot(Utils.getCurrentTime().replace(":", "")
				+ result.getName());
		MyVars.logger
				.info("=================================================================================");
		MyVars.logger.info("Test method success: " + result.getName() + " - "
				+ Utils.getCurrentTime());
		MyVars.logger
				.info("=================================================================================");
	}

	// Called when the test-method execution fails
	@Override
	public void onTestFailure(ITestResult result) {
		MyWebAction.takeScreenShot(Utils.getCurrentTime().replace(":", "")
				+ result.getName());
		MyVars.logger
				.info("=================================================================================");
		MyVars.logger.info("Test method failed: " + result.getName() + " - "
				+ Utils.getCurrentTime());
		MyVars.logger
				.info("=================================================================================");
	}

	// Called when the test-method is skipped
	@Override
	public void onTestSkipped(ITestResult result) {
		MyVars.logger
				.info("=================================================================================");
		MyVars.logger.info("Test method skipped: " + result.getName() + " - "
				+ Utils.getCurrentTime());
		MyVars.logger
				.info("=================================================================================");
	}

	// Called when the test-method fails within success percentage
	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// Leaving blank
	}

	// Called when the test in xml suite starts
	@Override
	public void onStart(ITestContext context) {
		MyVars.logger
				.info("=================================================================================");
		MyVars.logger.info("Test in a suite started: " + context.getName()
				+ " - " + Utils.getCurrentTime());
		MyVars.logger
				.info("=================================================================================");
	}

	// Called when the test in xml suite finishes
	@Override
	public void onFinish(ITestContext context) {
		MyVars.logger
				.info("=================================================================================");
		MyVars.logger.info("Test in a suite finished: " + context.getName()
				+ " - " + Utils.getCurrentTime());
		MyVars.logger
				.info("=================================================================================");
	}
}
