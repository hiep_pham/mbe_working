package bamboo;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.mbe.utilities.MyDataDriven;
import com.mbe.utilities.MyVars;
import com.mbe.utilities.MyWebAction;
import com.mbe.utilities.MyWebAction.TestEnviroment;
import com.mbe.utilities.MyWifiControler;

public class BambooTest {

	String locationPath = "src\\main\\resources\\locator.xls";
	String dataDrivenPath = "src\\main\\resources\\datadriven.xls";
	String product1 = "NLGs";
	String table1 = "VHA_DCB_NLGs_Subscription";

	MyDataDriven dt = new MyDataDriven(dataDrivenPath, product1);
	String appiumServer;
	String udid;
	
	@Parameters({ "appiumServer", "udid" })
	@BeforeClass
	public void prepare(String appiumServer, String udid) {
		// wifi control
		MyWifiControler wf = new MyWifiControler(appiumServer, udid);
		wf.turnOn(false);
		this.appiumServer = appiumServer;
		this.udid = udid;
	}

	@Parameters({ "appiumServer", "udid" })
	@BeforeMethod
	public void init(String appiumServer, String udid) {
		MyVars.softAssert = new SoftAssert();// reset verification result
		new MyWebAction(TestEnviroment.ANDROID, appiumServer, udid, null, null);
	}

	@AfterMethod
	public void tearDown() {
		MyVars.cellIndex[0] = MyVars.cellIndex[0] + 1;
		MyWebAction.tearDown();
	}

	@AfterClass
	public static void end() {
		// driver.quit();
	}

	@Test
	public void Bamboo_Test_script() {

		// Write test scripts at here
		// verify Portal page
		MyVars.logger.info("ACCESSING TO GOOGLE PAGE");
		MyWebAction.getURL("HTTP://GOOGLE.COM");
		MyWebAction.wait(5000);
		// get verification result
		MyVars.softAssert.assertAll();
	}
}
