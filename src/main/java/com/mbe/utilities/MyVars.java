package com.mbe.utilities;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.asserts.SoftAssert;

/**
 * @author Hiep Pham This class is used to define public vars
 */
public class MyVars {
	public static Properties locaton = new Properties(); // export locations
	public static Logger logger = Logger.getLogger(MyWebAction.class.getName());// log4j
	public static int[] cellIndex = { 0, 4 };// for updating portalUrl
	public static SoftAssert softAssert = new SoftAssert();
	public static final String chromeDriver = "src\\main\\chromedriver.exe";
}
