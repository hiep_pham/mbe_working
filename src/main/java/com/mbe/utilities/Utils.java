package com.mbe.utilities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

public class Utils {

	public static void main(String[] args) {
	}

	/**
	 * Resizes an image to a absolute width and height (the image may not be
	 * proportional)
	 * 
	 * @param inputImagePath
	 *            Path of the original image
	 * @param outputImagePath
	 *            Path to save the resized image
	 * @param scaledWidth
	 *            absolute width in pixels
	 * @param scaledHeight
	 *            absolute height in pixels
	 * @throws IOException
	 */
	public void resize(String inputImagePath, String outputImagePath,
			int scaledWidth, int scaledHeight) {

		try {
			// reads input image
			File inputFile = new File(inputImagePath);
			BufferedImage inputImage;
			inputImage = ImageIO.read(inputFile);
			// creates output image
			BufferedImage outputImage = new BufferedImage(scaledWidth,
					scaledHeight, inputImage.getType());

			// scales the input image to the output image
			Graphics2D g2d = outputImage.createGraphics();
			g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
			g2d.dispose();

			// extracts extension of output file
			String formatName = outputImagePath.substring(outputImagePath
					.lastIndexOf(".") + 1);

			// writes to output file
			ImageIO.write(outputImage, formatName, new File(outputImagePath));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 *      * Resizes an image by a percentage of original size (proportional).
	 *      * @param inputImagePath Path of the original image      * @param
	 * outputImagePath Path to save the resized image *@param extension image
	 * extension png of gif *@param percent a double number specifies percentage
	 * of the output image      * over the input image.      * @throws
	 * IOException      
	 */
	public void resize(String inputImagePath, String outputImagePath,
			String extension, double percent) {
		File inputFile = new File(inputImagePath);
		BufferedImage inputImage;
		try {
			inputImage = ImageIO.read(inputFile);
			ImageIO.write(inputImage, extension, inputFile);
			int scaledWidth = (int) (inputImage.getWidth() * percent);
			int scaledHeight = (int) (inputImage.getHeight() * percent);
			resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Returns the current time when the method is called
	public static String getCurrentTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd-HH:mm:ss:SSS");
		Date dt = new Date();
		return dateFormat.format(dt);
	}

	public String getMethodNameUsingObject() {
		String methodNameUsingObject = new Object() {
		}.getClass().getEnclosingMethod().getName();
		return methodNameUsingObject;
	}

	public String getMethodNameUsingThrowable() {
		StackTraceElement stackTraceElements[] = (new Throwable())
				.getStackTrace();
		return stackTraceElements[0].getMethodName();
	}

	public String getMethodNameUsingThread() {
		return Thread.currentThread().getStackTrace()[1].getMethodName();
	}

	public static String getValuePropertiesFile(String filePath, String property) {
		Properties prop = new Properties();
		InputStream input = null;
		String value = null;
		try {

			input = new FileInputStream(filePath);

			// load a properties file
			prop.load(input);

			// get the property value
			value = prop.getProperty(property);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
}
