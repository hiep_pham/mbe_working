package com.mbe.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.By;

/**
 * @author Hiep Pham This class is excel interaction library
 */
public class MyLocators {

	String xlsFilePath;
	String sheetName;

	public static void main(String[] args) throws Exception {

	}

	public MyLocators(String xlsFilePath, String sheetName) {
		super();
		this.xlsFilePath = xlsFilePath;
		this.sheetName = sheetName;
	}

	/**
	 * This method converts a String to By
	 * */
	public static By getLocation(String item) {
		String location = MyVars.locaton.getProperty(item);
		By by = null;
		if (location.startsWith("id=")) {
			by = By.id(location.substring(3));
		} else if (location.startsWith("name=")) {
			by = By.name(location.substring(5));
		} else if (location.startsWith("css=")) {
			by = By.cssSelector(location.substring(4));
		} else if (location.startsWith("link=")) {
			by = By.linkText(location.substring(5));
		} else if (location.startsWith("xpath=")) {
			by = By.xpath(location.substring(6));
		} else
			MyVars.logger.warn("NOT support " + location
					+ " location. Please check location library!");
		return by;
	}

	/**
	 * This method converts from excel data to Property
	 * */
	public Properties convertToProperty() {
		MyVars.logger.info("loading location library...");
		Sheet sheet = null;
		Workbook workbook = null;
		OutputStream output = null;

		try {
			workbook = Workbook.getWorkbook(new File(xlsFilePath));
			sheet = workbook.getSheet(sheetName);
		} catch (BiffException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			MyVars.logger.error("NOT complete loading location!", e1);
			e1.printStackTrace();
		}

		int startRow, endRow;
		startRow = 1;
		endRow = sheet.getRows();

		Properties property = new Properties();

		try {
			output = new FileOutputStream("location.properties");
			for (int i = startRow; i < endRow; i++) {
				property.setProperty(sheet.getCell(0, i).getContents(), sheet
						.getCell(1, i).getContents());
			}
			property.store(output, null);
		} catch (IOException io) {
			MyVars.logger.error("NOT complete converting location to property!", io);
			//io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		MyVars.logger.info("completed loading location!");
		return property;
	}
}
