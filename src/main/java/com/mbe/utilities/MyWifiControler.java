package com.mbe.utilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * @author Hiep Pham This class is used to interact to MESSAGING app
 */
public class MyWifiControler {

	private static WebDriver driver;
	// private String appiumServer = "127.0.0.1:4723";
	// private String udid = "0542b12b25234b6b";
	private String appiumServer;
	// private String udid = "SH43SWM10181";
	private String udid;

	public MyWifiControler(String appiumServer, String udid) {
		super();
		this.appiumServer = appiumServer;
		this.udid = udid;
	}

	/**
	 * @author pquochiep: action to turn on/off wifi. true for ON - false for
	 *         OFF
	 * */
	public void turnOn(boolean on) {
		MyVars.logger.info("preparing wifi");
		// prepare environment
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "Android Emulator");
		capabilities.setCapability("appPackage", "com.android.settings");
		capabilities.setCapability("appActivity",
				"com.android.settings.wifi.WifiSettings");
		capabilities.setCapability("udid", udid);
		try {
			driver = new RemoteWebDriver(new URL("http://" + appiumServer
					+ "/wd/hub"), capabilities);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(0);
		} catch (MalformedURLException e) {
			MyVars.logger.debug("NOT complete preparing test environment" + e);
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// action
		WebElement switchWidget = driver.findElement(By
				.xpath("*//android.widget.Switch"));

		if (on) {
			while (switchWidget.getAttribute("checked").equalsIgnoreCase(
					"false")) {
				MyVars.logger.info("Turning ON WIFI");
				switchWidget.click();
			}
		} else {
			while (switchWidget.getAttribute("checked")
					.equalsIgnoreCase("true")) {
				MyVars.logger.info("Turning OFF WIFI");
				switchWidget.click();
			}
		}
		MyWebAction.wait(3000);
		
		driver.quit();
	}
}
