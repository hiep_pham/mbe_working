package com.mbe.utilities;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyDBUtils {
	public static Connection connectDB(String baseUrl) {
		// Production
		String driver = "com.mysql.jdbc.Driver";
		String connection_url = "jdbc:mysql://dwh.mobileactive.net.au:3306/messaging_system";
		String username = "appium_user";
		String password = "shai2ooVroZoho4B";
		// Staging
		if (baseUrl.startsWith("http://st.")) {
			connection_url = "jdbc:mysql://stagingdb-nms.mobileactive.net.au:3306/messaging_system_stage";
			username = "appium_user";
			password = "AhHoo6uoaoDu3fie";
			MyVars.logger.info("connecting to Staging database...");
		} else {
			MyVars.logger.info("connecting to Production database...");
		}

		Connection connection = null;

		try {
			connection = MyDBUtils.createConnection(driver, connection_url,
					username, password);
			if (connection != null && connection instanceof Connection) {
				MyVars.logger.info("connection SUCCESSED!");
			}
		} catch (ClassNotFoundException e) {
			MyVars.logger.error("connection FAILED !!!");
			e.printStackTrace();
		} catch (SQLException e) {
			MyVars.logger.error("connection FAILED !!!");
			e.printStackTrace();
		}
		return connection;
	}

	public static Connection createConnection(String driver, String url,
			String username, String password) throws ClassNotFoundException,
			SQLException {
		Class.forName(driver);

		if ((username == null) || (password == null)
				|| (username.trim().length() == 0)
				|| (password.trim().length() == 0)) {
			return DriverManager.getConnection(url);
		} else {
			return DriverManager.getConnection(url, username, password);
		}
	}

	public static void close(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void rollback(Connection connection) {
		try {
			if (connection != null) {
				connection.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Map<String, Object>> map(ResultSet rs)
			throws SQLException {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		try {
			if (rs != null) {
				ResultSetMetaData meta = rs.getMetaData();
				int numColumns = meta.getColumnCount();
				while (rs.next()) {
					Map<String, Object> row = new HashMap<String, Object>();
					for (int i = 1; i <= numColumns; ++i) {
						String name = meta.getColumnName(i);
						Object value = rs.getObject(i);
						row.put(name, value);
					}
					results.add(row);
				}
			}
		} finally {
			close(rs);
		}

		return results;
	}
}