package com.mbe.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * @author Hiep Pham This class is excel interaction library
 */
public class MyDataDriven {

	String xlsFilePath;
	String sheetName;

	public static void main(String[] args) {
	}

	public MyDataDriven(String xlsFilePath, String sheetName) {
		super();
		this.xlsFilePath = xlsFilePath;
		this.sheetName = sheetName;
	}

	/**
	 * Write data to a cell
	 * */

	/**
	 * This method converts from excel data to Array[][]
	 */
	public String[][] getTableArray(String tableName) throws Exception {

		MyVars.logger.info("loading datadriven...");
		String[][] tabArray = null;
		Workbook workbook;
		Sheet sheet = null;
		Cell tableStart = null;

		try {
			workbook = Workbook.getWorkbook(new File(xlsFilePath));
			sheet = workbook.getSheet(sheetName);
			tableStart = sheet.findCell(tableName);
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			MyVars.logger.error("NOT complete loading datadriven!", e);
			e.printStackTrace();
		}

		int startRow, startCol, endRow, endCol, ci, cj;

		startRow = tableStart.getRow();
		startCol = tableStart.getColumn();

		Cell tableEnd = sheet.findCell(tableName, startCol + 1, startRow + 1,
				100, 64000, false);

		endRow = tableEnd.getRow();
		endCol = tableEnd.getColumn();

		tabArray = new String[endRow - startRow - 1][endCol - startCol - 1];
		ci = 0;

		for (int i = startRow + 1; i < endRow; i++, ci++) {
			cj = 0;
			for (int j = startCol + 1; j < endCol; j++, cj++) {
				tabArray[ci][cj] = sheet.getCell(j, i).getContents().trim();
			}
		}
		MyVars.logger.info("completed datadriven!");
		MyVars.cellIndex[0] = startRow + 1;
		return (tabArray);
	}

	public boolean writeToCell(int row, int col, String data) {
		boolean result = false;
		MyVars.logger.info("updating value to " + xlsFilePath + " -- "
				+ sheetName+"...");
		try {
			InputStream inp = new FileInputStream(xlsFilePath);
			org.apache.poi.ss.usermodel.Workbook wb = org.apache.poi.ss.usermodel.WorkbookFactory
					.create(inp);
			org.apache.poi.ss.usermodel.Sheet sheet = wb.getSheet(sheetName);

			org.apache.poi.ss.usermodel.Row r = sheet.getRow(row);
			org.apache.poi.ss.usermodel.Cell cell = r.createCell(col);

			cell.setCellValue(data);

			FileOutputStream fileOut = new FileOutputStream(xlsFilePath);
			wb.write(fileOut);
			fileOut.close();
			inp.close();
			result = true;
		} catch (Exception e) {
			MyVars.logger.error("NOT complete updating value to " + xlsFilePath
					+ " -- " + sheetName, e);
		}
		return result;
	}
}
