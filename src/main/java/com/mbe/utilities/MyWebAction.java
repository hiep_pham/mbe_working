package com.mbe.utilities;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import test.logger.MyEventListener;

public class MyWebAction {

	public static WebDriver driver;
	WebDriverEventListener eventListener = new MyEventListener();

	public static void main(String[] args) {
	}

	public static enum TestEnviroment {
		IOS, ANDROID, DESKTOP, IOSEmulator
	}

	/**
	 * Settup driver
	 * 
	 * @param test
	 *            : IOS, ANDROID, DESKTOP, IOSEmulator
	 * @param appiumServer
	 *            : null or 192.168.0.57
	 * @param udid
	 *            : null or udid of the target device
	 * @param appPackage
	 *            : package ID
	 * @param appActivity
	 *            : main activity of package
	 * */
	public MyWebAction(TestEnviroment test, String appiumServer, String udid,
			String appPackage, String appActivity) {
		if (driver != null) {
			driver.quit();
		}
		MyVars.logger.info("preparing test environment...");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		switch (test) {
		case IOS:
			capabilities.setCapability("deviceName", "iPhone");
			capabilities.setCapability("platformName", "iOS");
			capabilities.setCapability("platformVersion", "7.1");
			capabilities.setCapability("newCommandTimeout", "180");
			capabilities.setCapability("udid", udid);
			if (appPackage != null && appActivity != null) {
				capabilities.setCapability("appPackage", appPackage);
				capabilities.setCapability("appActivity", appActivity);
			} else
				capabilities.setCapability("browserName", "safari");
			try {
				driver = new EventFiringWebDriver(new RemoteWebDriver(new URL(
						"http://" + appiumServer + "/wd/hub"), capabilities))
						.register(eventListener);
				driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
				driver.manage().timeouts()
						.pageLoadTimeout(25, TimeUnit.SECONDS);
			} catch (MalformedURLException e) {
				MyVars.logger.error("NOT complete preparing test environment!"
						+ e);
				e.printStackTrace();
			}
			break;
		case IOSEmulator:
			capabilities.setCapability("deviceName", "iPhone Simulator");
			capabilities.setCapability("platformName", "iOS");
			capabilities.setCapability("platformVersion", "8.1");
			if (appPackage != null && appActivity != null) {
				capabilities.setCapability("appPackage", appPackage);
				capabilities.setCapability("appActivity", appActivity);
			} else
				capabilities.setCapability("browserName", "safari");
			try {
				driver = new RemoteWebDriver(new URL(
						"http://127.0.0.1:4723/wd/hub"), capabilities);
				driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
				driver.manage().timeouts()
						.pageLoadTimeout(25, TimeUnit.SECONDS);
			} catch (MalformedURLException e) {
				MyVars.logger.error("NOT complete preparing test environment!"
						+ e);
				e.printStackTrace();
			}
			break;
		case ANDROID:
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("deviceName", "Android Emulator");
			capabilities.setCapability("platformVersion", "4.4.2");
			capabilities.setCapability("newCommandTimeout", "180");
			capabilities.setCapability("udid", udid);
			if (appPackage != null && appActivity != null) {
				capabilities.setCapability("appPackage", appPackage);
				capabilities.setCapability("appActivity", appActivity);
			} else
				capabilities.setCapability(CapabilityType.BROWSER_NAME,
						"Chrome");
			try {
				driver = new EventFiringWebDriver(new RemoteWebDriver(new URL(
						"http://" + appiumServer + "/wd/hub"), capabilities))
						.register(eventListener);
				driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
				if (appPackage == null && appActivity == null) {
					driver.manage().timeouts()
							.pageLoadTimeout(25, TimeUnit.SECONDS);
				}
			} catch (MalformedURLException e) {
				MyVars.logger.error("NOT complete preparing test environment!"
						+ e);
				e.printStackTrace();
			}
			break;
		case DESKTOP:
			System.setProperty("webdriver.chrome.driver", MyVars.chromeDriver);
			driver = new EventFiringWebDriver(new ChromeDriver())
					.register(eventListener);
			// driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
			break;
		}
	}

	public static void getURL(String url) {
		MyVars.logger.info("going to " + url);
		try {
			driver.get(url);
		} catch (Exception e) {
			Assert.assertTrue(false, "CANNOT ACCESS  | " + url);
			MyVars.logger.error("CANNOT ACCESS  | " + url, e);
		}
	}

	public static WebElement findElement(String item) {
		By location = MyLocators.getLocation(item);
		return findElement(location);
	}

	public static WebElement findElement(By location) {
		WebElement element = null;
		try {
			element = driver.findElement(location);
			new Actions(driver).moveToElement(element).perform();
		} catch (Exception e) {
			MyVars.logger.debug("NOT FOUND | " + location, e);
		}
		return element;
	}

	public static List<WebElement> findElements(String item) {
		By location = MyLocators.getLocation(item);
		return findElements(location);
	}

	public static List<WebElement> findElements(By location) {
		MyVars.logger.info("finding findElements | " + location);
		List<WebElement> elementList = null;
		try {
			elementList = driver.findElements(location);
		} catch (Exception e) {
			MyVars.logger.debug("NOT FOUND | " + location, e);
		}
		return elementList;
	}

	public static void clickAnElement(String item) {
		By location = MyLocators.getLocation(item);
		clickAnElement(location);
	}

	public static void clickAnElement(By location) {
		MyVars.logger.info("clicking | " + location);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			assertElementIsVisible(location, "NOT FOUND | " + location);
			wait.until(ExpectedConditions.elementToBeClickable(location));
			driver.findElement(location).click();
		} catch (Exception e) {
			MyVars.logger.debug("NOT FOUND | " + location, e);
		}
	}

	public static void clickAnElement_app(By location) {
		MyVars.logger.info("clicking | " + location);
		try {
			driver.findElement(location).click();
		} catch (Exception e) {
			MyVars.logger.debug("NOT FOUND | " + location, e);
		}
	}

	public static void clickAnElementSafariNewTab(String item) {
		By location = MyLocators.getLocation(item);
		String link = null;
		MyVars.logger.info("clicking " + item + " | " + location);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(location));
			link = driver.findElement(location).getAttribute("href");
			driver.get(link);
		} catch (Exception e) {
			MyVars.logger.debug("NOT FOUND | " + location, e);
		}
	}

	public static void sendKeys(String item, String key, String message) {
		By location = MyLocators.getLocation(item);
		sendKeys(location, key, message);
	}

	public static void sendKeys(By location, String keys, String message) {
		MyVars.logger.info("sending keys | " + location + " | " + keys);
		assertElementIsVisible(location, message);
		driver.findElement(location).clear();
		driver.findElement(location).sendKeys(keys);
	}

	public static void assertElementExist(String item, String elementName) {
		Assert.assertTrue(!findElements(item).isEmpty(), "NOT FOUND | "
				+ elementName + " | " + item + "\n");
	}

	public static void assertElementExist(By location, String elementName) {
		Assert.assertTrue(!findElements(location).isEmpty(), "NOT FOUND | "
				+ elementName + " | " + location + "\n");
	}

	public static void assertElementIsNotExist(String item, String elementName) {
		assertFalse(!findElements(item).isEmpty(), "FOUND | " + elementName
				+ " | " + item + "\n");
	}

	public static void assertElementIsNotExist(By location, String elementName) {
		assertFalse(!findElements(location).isEmpty(), "FOUND | " + elementName
				+ " | " + location + "\n");
	}

	public static void assertElementIsVisible(String item, String message) {
		By location = MyLocators.getLocation(item);
		assertElementIsVisible(location, message);
	}

	public static void assertElementIsVisible(By location, String message) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(location));
			assertTrue(driver.findElement(location).isDisplayed());
		} catch (Exception e) {
			MyVars.logger.debug("NOT FOUND | " + message + " | " + location, e);
		}
	}

	public static void verifyEquals(String actual, String expected,
			String message) {
		MyVars.logger.debug("comparing: " + actual + " | " + expected);
		MyVars.softAssert.assertEquals(actual, expected, message + ": actual["
				+ actual + "] but expected [" + expected + "]");
	}

	public static void verifyTrue(boolean condition, String message) {
		MyVars.softAssert.assertTrue(condition, message);
	}
	
	public static void verifyFalse(boolean condition, String message) {
		MyVars.softAssert.assertFalse(condition, message);
	}

	public static void verifyElementExist(String item, String elementName) {
		MyVars.softAssert.assertTrue(!findElements(item).isEmpty(),
				"NOT FOUND | " + elementName + " | " + item + "\n");
	}

	public static void verifyElementExist(By location, String elementName) {
		MyVars.softAssert.assertTrue(!findElements(location).isEmpty(),
				"NOT FOUND | " + elementName + " | " + location + "\n");
	}

	public static void verifyElementIsNotExist(String item, String elementName) {
		MyVars.softAssert.assertFalse(!findElements(item).isEmpty(), "FOUND | "
				+ elementName + " | " + item + "\n");
	}

	public static void verifyElementIsNotExist(By location, String elementName) {
		MyVars.softAssert.assertFalse(!findElements(location).isEmpty(),
				"FOUND | " + elementName + " | " + location + "\n");
	}

	public static void verifyElementIsVisible(String item, String elementName) {
		By location = MyLocators.getLocation(item);
		verifyElementIsVisible(location, elementName);
	}

	public static void verifyElementIsVisible(By location, String elementName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions
					.visibilityOfAllElementsLocatedBy(location));
			MyVars.softAssert.assertTrue(driver.findElement(location)
					.isDisplayed(), "NOT FOUND | " + elementName + " | "
					+ location + "\n");
		} catch (Exception e) {
			MyVars.logger.debug(
					"NOT FOUND | " + elementName + " | " + location, e);
		}
	}

	public static String closeAlertAndGetItsText(boolean acceptNextAlert) {
		MyVars.logger.info("clicking to cofirm the alert | " + acceptNextAlert);
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	public static void switchToAnotherTab(int numberOfWindows) {
		MyVars.logger.info("executing switchToAnotherTab " + numberOfWindows);
		driver.switchTo().window(
				(new ArrayList<String>(driver.getWindowHandles())
						.get(numberOfWindows - 1).toString()));
	}

	public static boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			return false;
		}
	}

	/* close mobile-bookmark-bubble */
	public static void closeBubble() {
		// assertElementExist("bookmark_bubble.min.js script",
		// By.xpath("//script[contains(@src, 'bookmark_bubble.min.js')]"));
		MyVars.logger.info("executing closeBubble");
		((JavascriptExecutor) driver)
				.executeScript("google.bookmarkbubble.Bubble.prototype.destroy()");
		((JavascriptExecutor) driver)
				.executeScript("window.localStorage['BOOKMARK_DISMISSED_COUNT'] = 2;");
	}

	public static void printPageSource(String filePath) {
		MyVars.logger.info("executing printPageSource to " + filePath);
		PrintWriter pageSource;
		wait(5000);
		try {
			pageSource = new PrintWriter(filePath, "UTF-8");
			pageSource.print(driver.getPageSource());
			pageSource.close();
		} catch (FileNotFoundException e) {
			MyVars.logger.debug("NOT complete printPageSource to " + filePath,
					e);
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public static void takeScreenShot(String screenshotName) {
		if (driver != null) {
			String ext = "gif";
			// WebDriver dr = new Augmenter().augment(driver);
			File srcFile = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			File desFile = new File("target/screenshot/" + screenshotName + "."
					+ ext);
			if (!desFile.getParentFile().exists())
				desFile.getParentFile().mkdirs();
			// Util.resize(srcFile.getAbsolutePath(), desFile.getAbsolutePath(),
			// ext,1);
			BufferedImage inputImage = null;
			try {
				inputImage = ImageIO.read(srcFile);
				ImageIO.write(inputImage, ext, desFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			MyVars.logger.info("got screenshot: " + desFile.getAbsolutePath());
		}
	}

	public static void wait(int milisecond) {
		MyVars.logger.info("wait " + milisecond + "s.......................");
		try {
			Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void deleteAllCookies() {
		MyVars.logger.info("executing deleteAllCookies");
		driver.manage().deleteAllCookies();
		wait(2000);
	}

	public static void tearDown() {
		if (driver != null) {
			MyVars.logger.info("executing tearDown...");
			try {
				// for (String handle : driver.getWindowHandles()) {
				// driver.switchTo().window(handle).close();
				// }
				driver.quit();
			} catch (SessionNotFoundException e) {
				MyVars.logger.debug("NOT complete tearDown!");
				e.getStackTrace();
			}
		}
	}
}